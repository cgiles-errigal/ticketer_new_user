# README #

This script is useful for quickly setting up a new user for ticketer instances where other methods of user profile creation are unavailable.

### How to use it ###

* Open the ticketer_new_user.sql file in your favourite text editor
* Fill in the variables with the desired account details
* You can get an MD5 hash of your desired password by running `md5 -s 'PASSWORD'` on your local machine
* Save the file and copy it to the machine you want to run it on
* Run `mysql -uwriter -p<ticketer_new_user.sql`
* Log in to verify the account has been created