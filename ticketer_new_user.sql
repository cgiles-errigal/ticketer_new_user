use ticketer;
PREPARE create_user_stmt FROM 'INSERT INTO user (version,  email_address,first_name, last_name, login,password,primary_visibility_id) VALUES (1,?,?,?,?,?,1);';
PREPARE create_user_roles_stmt FROM 'INSERT INTO user_roles (user_id,role_id) SELECT user.id, user_role.id FROM user cross join user_role WHERE user.login = ?;';
SET @email = 'EMAIL HERE';
SET @fname = 'FIRST NAME HERE';
SET @sname = 'LAST NAME HERE';
SET @login = 'USERNAME HERE';
SET @pass = '{MD5}MD5 HASH OF PASSWORD HERE';
EXECUTE create_user_stmt USING @email, @fname, @sname, @login, @pass;
EXECUTE create_user_roles_stmt USING @login;